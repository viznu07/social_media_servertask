require('dotenv').config();
const { sequelize,User,User_data,Posts,Likes,Follow } = require('../../../../models');
const jwt = require('jsonwebtoken');
const bcryptjs = require("bcryptjs");
const {v4 : uuidv4} = require('uuid')

// create post
const CreatePost = ({ id, post, description, user_profile_id }) => {
    return new Promise(async (resolve, reject) => {
      try {
        sequelize.sync()
        const payload = {
          post,
          description,
          created_by: user_profile_id,
        };
        if (id && id.length > 0) {
          await Posts.destroy({
            where: {
              id,
            },
          });
          resolve("Post Deleted SuccessFully");
        } else {
          await Posts.create({ ...payload, id: uuidv4() });
          resolve("Post Created SuccessFully");
        }
      } catch (error) {
        console.log(error);
        reject(error);
      }
    });
};

// Like post
const LikePost =({id,post_id,isLiked,liked_by,user_profile_id})=>{
   return new Promise(async(resolve,reject) =>{
      try {
        sequelize.sync()
        const payload={
          id,
          post_id,
          isLiked,
          liked_by,
          created_by:user_profile_id
        }

        if (!id) {
          await Likes.create({ ...payload, id: uuidv4() });
          return resolve("Liked Created SucessFully");
        }
  
        await Likes.update(payload, {
          where: {
            id,
          },
        });
        return resolve("Liked Updated SucessFully");
      } catch (error) {
        console.log(error);
        reject(error);
      }
   })
}

// get post by created_by
const ViewPost = ({ id }) => {
  return new Promise(async (resolve, reject) => {
    try {
      sequelize.sync()
      const { count, rows } = await Posts.findAndCountAll({
        where: {
          created_by: id,
        },
        attributes: ["post", "description", "id", "createdAt"],
        include: [
          {
            model: Likes,
            as: "like",
            where: {
              created_by: id
            },
            attributes: ["isLiked","post_id","liked_by","id"],
            required: false,
          }],
        offset: 0,
        limit: 10,
        order: [["createdAt", "DESC"]],
      });
      resolve({
        count,
        data: rows,
      });
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

// View Followed Post Only
const ViewPrivatePost=({user_profile_id,id})=>{
  return new Promise(async(resolve,reject)=>{
    try {
      sequelize.sync()
      let is_following = await Follow.findOne({
        where:{
          followed_by:user_profile_id
        }
      })
      if(is_following?.following === id){
        const { count, rows } = await Posts.findAndCountAll({
          where: {
            created_by: id,
          },
          attributes: ["post", "description", "id", "createdAt"],
          offset: 0,
          limit: 10,
        });
        resolve({
          count,
          data: rows,
        });
      }
      resolve("No Posts available")
    } catch (error) {
      console.log(error);
      reject(error);
    }
  })
}

module.exports={CreatePost,LikePost,ViewPost,ViewPrivatePost}