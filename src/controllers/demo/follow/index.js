require('dotenv').config();
const { sequelize,Follow } = require('../../../../models');
const jwt = require('jsonwebtoken');
const bcryptjs = require("bcryptjs");
const {v4 : uuidv4} = require('uuid')

// Follow a User
const FollowUser=({followed_by,following,id,is_active})=>{
    return new Promise(async(resolve,reject)=>{
        try {
            sequelize.sync()
            const payload={
                followed_by,
                following,
                is_active
            }
            if (id && id.length > 0) {
                await Follow.destroy({
                  where: {
                    id,
                  },
                });
                resolve("Unfollowed SuccessFully");
              } else {
                await Follow.create({ ...payload, id: uuidv4() });
                resolve("Followed SuccessFully");
              }
            
        } catch (error) {
            
        }
    })
}

module.exports={FollowUser}