require('dotenv').config();
const { sequelize,User,User_data } = require('../../../../models');
const jwt = require('jsonwebtoken');
const bcryptjs = require("bcryptjs");

// Create user
const CreateUser = ({ firstName, lastName, email}) => {
  return new Promise(async (resolve, reject) => {
    try {
      sequelize.sync()
      await User.create({ firstName, lastName, email });
      resolve(true);
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

// Sign up
const Signup = (payload) => {
  return new Promise(async (resolve, reject) => {
    try {
      sequelize.sync()
      await User_data.create(payload);
      resolve(true);
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

// Login 
const Login = ({ Name, password }) => {
  return new Promise(async (resolve, reject) => {
    try {
      sequelize.sync();
      var user_details = await User_data.findOne({
        where: { Name: Name },
      });
      if (!user_details) {
        return resolve({ type: "Error", message: "User does not exists" });
      }
      var pass = await bcryptjs.compare(password, user_details.password);
      if (!pass) {
        return resolve({ type: "Error", message: "Wrong Password" });
      }
      // token
      var userToken = jwt.sign(user_details.id,process.env.JWT_SECRET);
      resolve({ type:"Success",authToken: userToken, details: user_details });
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

module.exports = { CreateUser,Signup,Login }