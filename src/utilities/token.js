const jwt = require('jsonwebtoken')

const verify = (token) => {
    return new Promise((resolve, reject) => {
      try {
        let payload = jwt.verify(token, process.env.JWT_SECRET);
        resolve(payload);
      } catch (error) {
        reject(error);
      }
    });
};

 const decode = (token) => {
    return jwt.decode(token, configure.jwt_key);
  };

module.exports = {verify,decode}