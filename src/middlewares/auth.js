require('dotenv').config();
const ErrorResponse = require("../utilities/errorHandler");
const jwt = require("jsonwebtoken");
const {verify} = require('../utilities/token')

const protect = async (req, res, next) => {
  try {
    if (
      req.headers.authorization &&
      req.headers.authorization.split(" ")[1] &&
      req.headers.authorization.split(" ")[1] != "null" &&
      req.headers.authorization.split(" ")[1] != "undefined"
    ) {
      const token = req.headers.authorization.split(" ")[1];
      const payload = await verify(token);
      return next();
    }
    return next({
      code: 403,
      message: "You are not an authorized user!",
    });
  } catch (err) {
    console.log(err);
    return next({
      code: 400,
      message: err.message,
    });
  }
};


module.exports = protect
