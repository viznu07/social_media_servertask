require('dotenv').config();
const express = require('express');
const { CreatePost,LikePost,ViewPrivatePost } = require("../../../controllers/demo/posts")

const router = express.Router();

router.post("/upsert_post", async (req, res, next) => {
  try {
    let data = await CreatePost(req?.body);
    res.status(200).send({ type: "success",data });
  } catch (err) {
    console.log(err);
    next({
      code: 500,
      message: err,
    });
  }
});

router.post("/like_post", async (req, res, next) => {
  try {
   let data = await LikePost(req?.body);
    res.status(200).send({ type: "success",data });
  } catch (err) {
    console.log(err);
    next({
      code: 500,
      message: err,
    });
  }
});

router.post("/view_private_post", async (req, res, next) => {
  try {
   let data = await ViewPrivatePost(req?.body);
    res.status(200).send({ type: "success",data });
  } catch (err) {
    console.log(err);
    next({
      code: 500,
      message: err,
    });
  }
});



module.exports = router;