const express = require('express');
const router = express.Router();
const {FollowUser} = require('../../../controllers/demo/follow');
// create follow
router.post("/follow_user", async (req, res, next) => {
  try {
    const data = await FollowUser(req?.body);
    res.status(200).send({ type: "success", data });
  } catch (err) {
    console.log(err);
    next({
      code: 500,
      message: err,
    });
  }
});

module.exports = router;
