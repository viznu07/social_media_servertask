require('dotenv').config();
const express = require('express');
const { CreateUser,Signup,Login } = require('../../../controllers/demo/userProfiles');
const {ViewPost} = require('../../../controllers/demo/posts')
const jwt = require('jsonwebtoken');
const bcryptjs = require("bcryptjs");
const {v4 : uuidv4} = require('uuid')

const router = express.Router();

router.post("/register", async (req, res, next) => {
  try {
    let data = CreateUser(req.body);
    res.status(200).send({ type: "success", data });
  } catch (err) {
    console.log(err);
    next({
      code: 500,
      message: err,
    });
  }
});

router.post("/signup", async (req, res, next) => {
  try {
    // hash the password
    var hash = await bcryptjs.hash(req.body.password, 10);
    const value = {
      id: uuidv4(),
      Name: req?.body?.name,
      email: req?.body?.email,
      password: hash,
    };
    const data = await Signup(value);
    res.status(200).send({ type: "success", data });
  } catch (err) {
    console.log(err);
    next({
      code: 500,
      message: err.message,
    });
  }
});

router.post("/login",async (req,res,next)=>{
  try {
    const value = {
      Name: req?.body?.name,
      password: req?.body?.password,
    };
    const data = await Login(value)
    res.status(200).send(data)
  } catch (error) {
    console.log(err);
    next();
  }
 
});

router.post("/view_public_post", async (req, res, next) => {
  try {
   let data = await ViewPost(req?.body);
    res.status(200).send({ type: "success",data });
  } catch (err) {
    console.log(err);
    next({
      code: 500,
      message: err,
    });
  }
});

module.exports = router