'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    queryInterface.renameColumn('Posts', 'descripion', 'description');
  },

  async down (queryInterface, Sequelize) {
    queryInterface.renameColumn('Posts', 'descripion', 'description');
  }
};
