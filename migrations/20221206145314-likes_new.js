'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn("Likes", "created_by", {
        type: Sequelize.STRING,
        references: { model: "User_data", key: "id" },
      }),
    ]);
  },

  async down (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn("Likes", "created_by", {
        type: Sequelize.STRING,
        references: { model: "User_data", key: "id" },
      }),
    ]);
  }
};
