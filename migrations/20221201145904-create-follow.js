'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Follows', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING
      },
      followed_by: {
        type: Sequelize.STRING,
        references: { model: "User_data", key: "id" },
      },
      following: {
        type: Sequelize.STRING,
        references: { model: "User_data", key: "id" },
      },
      is_active: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Follows');
  }
};