'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.changeColumn("Likes", "id", {
        type: Sequelize.STRING,
        primaryKey:true,
        allowNull: false,
      }),
    ]);
  },

  async down (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.changeColumn("Likes", "id", {
        type: Sequelize.STRING,
        primaryKey:true,
        allowNull: false,
      }),
    ]);
  }
};
