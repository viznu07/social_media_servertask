const express = require('express');
const body = require('body-parser');
const path = require('path');
const routers = require('./src/routers')
const OS = require('os')
const db = require('./db/config')


const app = express();

const PORT = process.env.PORT || 5000;
app.set("port", PORT);
app.use(express.json());


app.use("/api/v1",routers);

app.get('/',(req,res)=>{
    const status = {
    uptime: process.uptime(),
    message: "Server is running...",
    process_id: process.pid,
    dateTime: new Date(),
    platform: OS.platform(),
    processor: OS.cpus()[0].model,
    architecture: OS.arch(),
    thread_count: OS.cpus().length,
    total_memory: `${(OS.totalmem() / 1e9).toFixed(2)} GB`,
    free_memory: `${(OS.freemem() / 1e9).toFixed(2)} GB`,
  };

  res.status(200).send(status);
})

app.listen(PORT, async () => {
    try {
      console.log("Server is running at port : " + PORT);
      await db.authenticate();
      console.log("Connection has been established successfully.");
    } catch (error) {
      console.error("Unable to connect to the database:", error);
    }
  });
  