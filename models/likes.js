'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Likes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Likes.belongsTo(models.Posts, {
        onDelete: "CASCADE",
        foreignKey: "id",
      });
      Likes.belongsTo(models.User_data, {
        onDelete: "CASCADE",
        foreignKey: "id",
      });
    }
  }
  Likes.init({
    isLiked: DataTypes.BOOLEAN,
    liked_by: DataTypes.STRING,
    post_id: DataTypes.STRING,
    created_by:DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Likes',
  });
  return Likes;
};