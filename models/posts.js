'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Posts extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Posts.belongsTo(models.User_data, {
        onDelete: "CASCADE",
        foreignKey: "created_by",
      });
      Posts.hasMany(models.Likes, {
        onDelete: "CASCADE",
        foreignKey: "post_id",
        as: "like",
      });
    }
  }
  Posts.init({
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    post: DataTypes.STRING,
    description: DataTypes.STRING,
    isLiked: DataTypes.BOOLEAN,
    created_by:DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Posts',
  });
  return Posts;
};